import { Controller, Get, Inject, OnModuleInit } from '@nestjs/common';
import { ClientGrpc } from '@nestjs/microservices';
import { lastValueFrom, Observable, ReplaySubject, toArray } from 'rxjs';
import { HeroById } from './interfaces/hero-by-id.interface';
import { HeroService } from './interfaces/hero-service.interface';
import { Hero } from './interfaces/hero.interface';

@Controller()
export class AppController implements OnModuleInit {
  private heroService: HeroService;
  constructor(@Inject('HERO_PACKAGE') private clientHero: ClientGrpc) {}
  onModuleInit() {
    this.heroService = this.clientHero.getService<HeroService>('HeroesService');
  }

  @Get('test-get-by-id')
  async getByIdTest(): Promise<Hero> {
    const data: HeroById = { id: 1 };
    const hero$ = this.heroService.findOne(data);
    return await lastValueFrom(hero$);
  }

  @Get('test-get-many')
  getMany(): Observable<Hero[]> {
    const ids$ = new ReplaySubject<HeroById>();
    ids$.next({ id: 1 });
    ids$.next({ id: 2 });
    ids$.complete();

    const heroList$ = this.heroService.findMany(ids$.asObservable());
    const list = heroList$.pipe(toArray());
    list.subscribe((x) => {
      console.log(x[0]);
    });
    return list;
  }
}
